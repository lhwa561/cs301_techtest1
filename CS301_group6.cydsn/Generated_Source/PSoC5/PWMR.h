/*******************************************************************************
* File Name: PWMR.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_PWMR_H) /* Pins PWMR_H */
#define CY_PINS_PWMR_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "PWMR_aliases.h"

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 PWMR__PORT == 15 && ((PWMR__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

/**
* \addtogroup group_general
* @{
*/
void    PWMR_Write(uint8 value);
void    PWMR_SetDriveMode(uint8 mode);
uint8   PWMR_ReadDataReg(void);
uint8   PWMR_Read(void);
void    PWMR_SetInterruptMode(uint16 position, uint16 mode);
uint8   PWMR_ClearInterrupt(void);
/** @} general */

/***************************************
*           API Constants        
***************************************/
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup driveMode Drive mode constants
     * \brief Constants to be passed as "mode" parameter in the PWMR_SetDriveMode() function.
     *  @{
     */
        #define PWMR_DM_ALG_HIZ         PIN_DM_ALG_HIZ
        #define PWMR_DM_DIG_HIZ         PIN_DM_DIG_HIZ
        #define PWMR_DM_RES_UP          PIN_DM_RES_UP
        #define PWMR_DM_RES_DWN         PIN_DM_RES_DWN
        #define PWMR_DM_OD_LO           PIN_DM_OD_LO
        #define PWMR_DM_OD_HI           PIN_DM_OD_HI
        #define PWMR_DM_STRONG          PIN_DM_STRONG
        #define PWMR_DM_RES_UPDWN       PIN_DM_RES_UPDWN
    /** @} driveMode */
/** @} group_constants */
    
/* Digital Port Constants */
#define PWMR_MASK               PWMR__MASK
#define PWMR_SHIFT              PWMR__SHIFT
#define PWMR_WIDTH              1u

/* Interrupt constants */
#if defined(PWMR__INTSTAT)
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in PWMR_SetInterruptMode() function.
     *  @{
     */
        #define PWMR_INTR_NONE      (uint16)(0x0000u)
        #define PWMR_INTR_RISING    (uint16)(0x0001u)
        #define PWMR_INTR_FALLING   (uint16)(0x0002u)
        #define PWMR_INTR_BOTH      (uint16)(0x0003u) 
    /** @} intrMode */
/** @} group_constants */

    #define PWMR_INTR_MASK      (0x01u) 
#endif /* (PWMR__INTSTAT) */


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define PWMR_PS                     (* (reg8 *) PWMR__PS)
/* Data Register */
#define PWMR_DR                     (* (reg8 *) PWMR__DR)
/* Port Number */
#define PWMR_PRT_NUM                (* (reg8 *) PWMR__PRT) 
/* Connect to Analog Globals */                                                  
#define PWMR_AG                     (* (reg8 *) PWMR__AG)                       
/* Analog MUX bux enable */
#define PWMR_AMUX                   (* (reg8 *) PWMR__AMUX) 
/* Bidirectional Enable */                                                        
#define PWMR_BIE                    (* (reg8 *) PWMR__BIE)
/* Bit-mask for Aliased Register Access */
#define PWMR_BIT_MASK               (* (reg8 *) PWMR__BIT_MASK)
/* Bypass Enable */
#define PWMR_BYP                    (* (reg8 *) PWMR__BYP)
/* Port wide control signals */                                                   
#define PWMR_CTL                    (* (reg8 *) PWMR__CTL)
/* Drive Modes */
#define PWMR_DM0                    (* (reg8 *) PWMR__DM0) 
#define PWMR_DM1                    (* (reg8 *) PWMR__DM1)
#define PWMR_DM2                    (* (reg8 *) PWMR__DM2) 
/* Input Buffer Disable Override */
#define PWMR_INP_DIS                (* (reg8 *) PWMR__INP_DIS)
/* LCD Common or Segment Drive */
#define PWMR_LCD_COM_SEG            (* (reg8 *) PWMR__LCD_COM_SEG)
/* Enable Segment LCD */
#define PWMR_LCD_EN                 (* (reg8 *) PWMR__LCD_EN)
/* Slew Rate Control */
#define PWMR_SLW                    (* (reg8 *) PWMR__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define PWMR_PRTDSI__CAPS_SEL       (* (reg8 *) PWMR__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define PWMR_PRTDSI__DBL_SYNC_IN    (* (reg8 *) PWMR__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define PWMR_PRTDSI__OE_SEL0        (* (reg8 *) PWMR__PRTDSI__OE_SEL0) 
#define PWMR_PRTDSI__OE_SEL1        (* (reg8 *) PWMR__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define PWMR_PRTDSI__OUT_SEL0       (* (reg8 *) PWMR__PRTDSI__OUT_SEL0) 
#define PWMR_PRTDSI__OUT_SEL1       (* (reg8 *) PWMR__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define PWMR_PRTDSI__SYNC_OUT       (* (reg8 *) PWMR__PRTDSI__SYNC_OUT) 

/* SIO registers */
#if defined(PWMR__SIO_CFG)
    #define PWMR_SIO_HYST_EN        (* (reg8 *) PWMR__SIO_HYST_EN)
    #define PWMR_SIO_REG_HIFREQ     (* (reg8 *) PWMR__SIO_REG_HIFREQ)
    #define PWMR_SIO_CFG            (* (reg8 *) PWMR__SIO_CFG)
    #define PWMR_SIO_DIFF           (* (reg8 *) PWMR__SIO_DIFF)
#endif /* (PWMR__SIO_CFG) */

/* Interrupt Registers */
#if defined(PWMR__INTSTAT)
    #define PWMR_INTSTAT            (* (reg8 *) PWMR__INTSTAT)
    #define PWMR_SNAP               (* (reg8 *) PWMR__SNAP)
    
	#define PWMR_0_INTTYPE_REG 		(* (reg8 *) PWMR__0__INTTYPE)
#endif /* (PWMR__INTSTAT) */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_PWMR_H */


/* [] END OF FILE */
