/* ========================================
 * Fully working code: 
 * PWM      : 
 * Encoder  : 
 * ADC      :
 * USB      : port displays speed and position.
 * CMD: "PW xx"
 * Copyright Univ of Auckland, 2016
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF Univ of Auckland.
 *
 * ========================================
*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <project.h>
//* ========================================
#include "defines.h"
#include "vars.h"
//* ========================================
void usbPutString(char *s);
void usbPutChar(char c);
void handle_usb();
//* ========================================
int MODE;

int ADC_Out0, ADC_Out1, ADC_Out2, ADC_Out3, ADC_Out4, ADC_Out5, ADC_Out6;

int A1itr, A1OnIsTrue;
int A2itr, A2OnIsTrue;
int A3itr, A3OnIsTrue;
int A4itr, A4OnIsTrue;
int A5itr, A5OnIsTrue;
int A0itr, A0OnIsTrue;

char* outputline;
int v0, v1, v2, v3, v4, v5,v6;
int m1, m2;
int rx0, rx1, print;
int enable, rxindex;

int i = 0;
int a0_array[75];
int a1_array[75];
int a2_array[75];
int a3_array[75];
int a4_array[75];
int a5_array[75];
int a6_array[75];

_Bool a0_increase = 1;
_Bool a0_decrease = 1;
_Bool a1_increase = 1;
_Bool a1_decrease = 1;
_Bool a2_increase = 1;
_Bool a2_decrease = 1;
_Bool a3_increase = 1;
_Bool a3_decrease = 1;
_Bool a4_increase = 1;
_Bool a4_decrease = 1;
_Bool a5_increase = 1;
_Bool a5_decrease = 1;

int a0_high = 0;
int a0_low = 0;
int a1_high = 0;
int a1_low = 0;
int a2_high = 0;
int a2_low = 0;
int a3_high = 0;
int a3_low = 0;
int a4_high = 0;
int a4_low = 0;
int a5_high = 0;
int a5_low = 0;

char dest[5];
char a0[10];
char a1[10];
char a2[10];
char a3[10];
char a4[10];
char a5[10];
char a6[10];

typedef struct rx_data {
	int8            rssi;	
    uint8           index;			// index number of packet. incremented number
	uint16			robot_xpos;	 	// 
	uint16			robot_ypos;		//
    uint16         robot_orientation;
	uint16			g0_xpos;		//
	uint16			g0_ypos;		//
	uint16			g0_speed;		//
	uint16		g0_direction;	//
	uint16			g1_xpos;		//
	uint16			g1_ypos;		//
	uint16			g1_speed;		//
    uint16		g1_direction;	//
    uint16			g2_xpos;		//
    uint16			g2_ypos;		//
    uint16			g2_speed;		//
    uint16		g2_direction;	//
} rxtype1;

struct rx_data rx_data_state;

CY_ISR( ADC_ISR )
{
    #ifdef ADC_ISR_INTERRUPT_CALLBACK
        ADC_ISR_InterruptCallback();
    #endif // ADC_SAR_Seq_ISR_INTERRUPT_CALLBACK 
    // ***********************************************************************
    // * Custom Code
    // * - add user ISR code between the following #START and #END tags
    // ************************************************************************ 
    // `#START MAIN_SEQ_ADC_ISR` 
    //    ADC_StopConvert();
    
       // ADC_StartConvert();
        
        
        ADC_Out0 = ADC_GetResult16(0);
        ADC_Out1 = ADC_GetResult16(1);
        ADC_Out2 = ADC_GetResult16(2);
        ADC_Out3 = ADC_GetResult16(3);
        ADC_Out4 = ADC_GetResult16(4);
        ADC_Out5 = ADC_GetResult16(5);
        ADC_Out6 = ADC_GetResult16(6);
        
        v6 = ADC_CountsTo_mVolts(ADC_Out6);
        v6 = v6*4.58;
        a6_array[i] = v6;
        
        v0 = ADC_CountsTo_mVolts(ADC_Out0);
        v0 = v0 * 4.58;
        a0_array[i] = v0;
        
        v1 = ADC_CountsTo_mVolts(ADC_Out1);
        v1 = v1 * 4.58;
        a1_array[i] = v1;
        
        v2 = ADC_CountsTo_mVolts(ADC_Out2);
        v2 = v2 * 4.58;
        a2_array[i] = v2;
        
        v3 = ADC_CountsTo_mVolts(ADC_Out3);
        v3 = v3 * 4.58;
        a3_array[i] = v3;
        
        v4 = ADC_CountsTo_mVolts(ADC_Out4);
        v4 = v4 * 4.58;
        a4_array[i] = v4;
        
        v5 = ADC_CountsTo_mVolts(ADC_Out5);
        v5 = v5 * 4.58;
        a5_array[i] = v5;
        
        
        if (i >= 74) {
            i = 0;
        } else {
            i++;}
       
    //    ADC_StartConvert();
    // `#END`
        
}

CY_ISR(isr_RX)
{ 
/*    
    rx1 = UART_ReadRxData();
    if (rx0 == rx1) {
        print = 0;
    }
    else {
        rx0 = rx1;
        print = 1;
    }
*/  
    rx0 = UART_ReadRxData();
/*  if (rx0 == '#') {
        enable = 1;  
        rxindex = 0;
    }
    if ((rx0 == '!') || (rxindex > 17)) {
        enable = 0;
        print = 0;
    }
    
    if (enable == 1) {
        //rx0 = UART_ReadRxData();
        
        if (rx0 == ',') {
            rxindex++;
        }
        else {
            if (rxindex == 0) {
                rxindex++;    
            }
            else if (rxindex == 1) {
                rx_data_state.rssi = rx0;
            }
            else if (rxindex == 2) {
                rx_data_state.index = rx0;
            }
            else if (rxindex == 3) {
                rx_data_state.robot_xpos = rx0;
            }
            else if (rxindex == 4) {
                rx_data_state.robot_ypos = rx0;
            }
            else if (rxindex == 5) {
                rx_data_state.robot_orientation = rx0;
            }
            else if (rxindex == 6) {
                rx_data_state.g0_xpos = rx0;
            }
            else if (rxindex == 7) {
                rx_data_state.g0_ypos = rx0;
            }
            else if (rxindex == 8) {
                rx_data_state.g0_speed = rx0;
            }
            else if (rxindex == 9) {
                rx_data_state.g0_direction = rx0;
            }
            else if (rxindex == 10) {
                rx_data_state.g1_xpos = rx0;
            }
            else if (rxindex == 11) {
                rx_data_state.g1_ypos = rx0;
            }
            else if (rxindex == 12) {
                rx_data_state.g1_speed = rx0;
            }
            else if (rxindex == 13) {
                rx_data_state.g1_direction = rx0;
            }
            else if (rxindex == 14) {
                rx_data_state.g2_xpos = rx0;
            }
            else if (rxindex == 15) {
                rx_data_state.g2_ypos = rx0;
            }
            else if (rxindex == 16) {
                rx_data_state.g2_speed = rx0;
            }
            else if (rxindex == 17) {
                rx_data_state.g2_direction = rx0;
                rxindex++;
            }
            else {
                enable = 0;
            }
        }
    }
    */
    //usbPutString("#%i,%i,[%i,%i,%i],[%i,%i,%i,%i],[%i,%i,%i,%i],[%i,%i,%i,%i]", rx_data_state.rssi, rx_data_state.index, rx_data_state.robot_xpos, rx_data_state.robot_ypos, rx_data_state.robot_orientation, rx_data_state.g0_xpos, rx_data_state.g0_ypos, rx_data_state.g0_speed, rx_data_state.g0_direction, rx_data_state.g1_xpos, rx_data_state.g1_ypos, rx_data_state.g1_speed, rx_data_state.g1_direction, rx_data_state.g2_xpos, rx_data_state.g2_ypos, rx_data_state.g2_speed, rx_data_state.g2_direction);
}   


int main()
{
    
// --------------------------------    
// ----- INITIALIZATIONS ----------
    CYGlobalIntEnable;
    print = 0;
    enable = 0;
    PWM_1_Start();
    PWM_2_Start();
    QuadDec_M1_Start();
    QuadDec_M2_Start();
    //CHANGE THIS TO CHANGE MODE !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    MODE = 1;     // MODE 0 IS STRAIGHT LINE TEST
                  // MODE 1 IS CURVY WIGGLY LINE TEST
                  // MODE 2 IS RIGHT ANGLE TURN TEST
                  // MODE 3 IS IGNORE SIDES TEST
                  // MODE 4 IS RF TEST
                  // MODE 5 is ADC TEST
    //MAKE SURE YOU CHANGED TO THE MODE YOU WANT !!!!!!!!!!!!!!!!!
    A5OnIsTrue = 1;
    
    if (MODE == 4) {
        UART_Start();
        isrRF_RX_StartEx(isr_RX);
    }
// ------USB SETUP ----------------    
#ifdef USE_USB    
    USBUART_Start(0,USBUART_5V_OPERATION);
#endif     
    ADC_Start();
    ADC_IRQ_StartEx(ADC_ISR);
    ADC_StartConvert();
    
    PWM_1_WriteCompare(200);
    PWM_2_WriteCompare(56);
    QuadDec_M2_SetCounter(QuadDec_M1_GetCounter());
    
  
    for(;;)
    {
        //rx0 = UART_ReadRxData();  
        //PWM_1_WriteCompare(250);
        //PWM_2_WriteCompare(0);
        QuadDec_M2_SetCounter(QuadDec_M1_GetCounter());
        m1 = QuadDec_M1_GetCounter();
        m2 = QuadDec_M2_GetCounter();
        
        //error = m1/m2;
        //slave = slave*error;
        //LED_Write(1);
        //usbPutChar(12);
 
        
        
        //usbPutChar(rx0 + '0');

    if (MODE == 4) {    
        if (rx0 == rx1 || rx0 == 'N' || rx0 == 'o' || rx0 == 'R' || rx0 == 'x') {
            
        }
        else {
            rx1 = rx0;
            if (rx0 == '#') {
                usbPutChar(rx0);
            }
            else if (rx0 == '\n' || rx0 == '!') {
                usbPutChar('\r');
                usbPutChar('\n');
                
            }
            else if (rx0 == ',') {
                usbPutChar(',');
            }
            else {
                usbPutChar(rx0);
            }
        }
    }    
    
    /*    
        m1 = QuadDec_M1_GetCounter();
        
        usbPutString("M1 value is: ");
        itoa(m2, dest, 10);
        usbPutString(dest);
        usbPutString("          "); 
        
        if (m1 >= 999) {
            QuadDec_M1_SetCounter(0);
        }
        
        m2 = QuadDec_M2_GetCounter();
        itoa(m2, dest, 10);
        usbPutString("M2 value is: ");
        usbPutString(dest);
        usbPutChar('\r');
        usbPutChar('\n'); 
        
        if (m2 >= 999) {
            QuadDec_M2_SetCounter(0);
        }
   */     
        if (MODE == 0) {
            A5itr = 0;
            A5OnIsTrue = 0;
            for (A5itr = 0; A5itr < 75; A5itr++) {
                if (a5_array[A5itr] > 2650 || a5_array[A5itr] < 2350) { //2750> <2250 too big never succeeds 2700> <2300 not big enough  for a2_array
                    // A2,A5 >2700 <2300
                    // A1, >2800 <2200
                    //usbPutString("A5 is OFF\r\n");
                    A5OnIsTrue = 1;
                    break;
                }
                else {
                    A5OnIsTrue = 0;
                }
            }
                       
            if (A5OnIsTrue == 0) {
                PWM_1_WriteCompare(125);
                PWM_2_WriteCompare(125);
                //usbPutString("WE ARE CHANGING CMP VALUE LMAO Y U NO WORK FGT\r\n");
                
            }
            /*else {
                PWM_1_WriteCompare(250);
                PWM_2_WriteCompare(0);
                QuadDec_M2_SetCounter(QuadDec_M1_GetCounter());
            }*/
        }
        else if (MODE == 1) {
            if (A1OnIsTrue == 1) {
                PWM_1_WriteCompare(180);
                PWM_2_WriteCompare(125);
            }
            else if (A5OnIsTrue == 1) {
                PWM_1_WriteCompare(125);
                PWM_2_WriteCompare(76);
            }
            else {
                PWM_1_WriteCompare(180);
                PWM_2_WriteCompare(76);
                QuadDec_M2_SetCounter(QuadDec_M1_GetCounter());    
            }
        }
        else if (MODE == 2) {
            
        }
        else if (MODE == 3) {
            if (A2OnIsTrue == 0) {
                PWM_1_WriteCompare(76);
                PWM_2_WriteCompare(180);
                QuadDec_M2_SetCounter(QuadDec_M1_GetCounter());
            }
            else {
                if (A1OnIsTrue == 0) {
                    PWM_1_WriteCompare(100);
                    PWM_2_WriteCompare(190);                    
                }
            }
        }
    
            
        if (ADC_IsEndConversion(ADC_RETURN_STATUS) != 0) {
            A1itr = 0;
            A1OnIsTrue = 0;
            for (A1itr = 0; A1itr < 75; A1itr++) {
                if (a1_array[A1itr] > 2800 || a1_array[A1itr] < 2200) { //2750> <2250 too big never succeeds 2700> <2300 not big enough  for a2_array
                    // A2,A5 >2700 <2300
                    // A1, >2800 <2200
                    //usbPutString("A1 is OFF        ");
                    A1OnIsTrue = 1;
                    break;
                }
                else {
                    A1OnIsTrue = 0;
                }
            }
            
            if (A1OnIsTrue == 0) {
                //usbPutString("A1 is ON        ");
            }
            
            A2itr = 0;
            A2OnIsTrue = 0;
            for (A2itr = 0; A2itr < 75; A2itr++) {
                if (a2_array[A2itr] > 2700 || a2_array[A2itr] < 2300) { //2750> <2250 too big never succeeds 2700> <2300 not big enough  for a2_array
                    // A2,A5 >2700 <2300
                    // A1, >2800 <2200
                    //usbPutString("A2 is OFF        ");
                    A2OnIsTrue = 1;
                    break;
                }
                else {
                    A2OnIsTrue = 0;
                }
            }
            
            if (A2OnIsTrue == 0) {
                //usbPutString("A2 is ON        ");
            }
            
            if (MODE != 0) {
                A5itr = 0;
                A5OnIsTrue = 0;
                for (A5itr = 0; A5itr < 75; A5itr++) {
                    if (a5_array[A5itr] > 2700 || a5_array[A5itr] < 2300) { //2750> <2250 too big never succeeds 2700> <2300 not big enough  for a2_array
                        // A2,A5 >2700 <2300
                        // A1, >2800 <2200
                        //usbPutString("A5 is OFF\r\n");
                        A5OnIsTrue = 1;
                        break;
                    }
                    else {
                        A5OnIsTrue = 0;
                    }
                }
                
                if (A5OnIsTrue == 0) {
                    //usbPutString("A5 is ON\r\n");
                }
            }
            
            
            
            //usbPutString("v0 is: ");            
            //itoa(v0, a0, 10);
            //usbPutString(a0);           
            //usbPutString("       ");
        if (MODE == 5) { 
            usbPutString("Battery Voltage is: ");
            itoa(v6, a6, 10);
            usbPutString(a6);
            usbPutChar('\r');
            usbPutChar('\n');
           /* usbPutString("v1 is: ");
            itoa(v1, a1, 10);
            usbPutString(a1);
            
            usbPutString("       ");
            
            usbPutString("v2 is: ");
            itoa(v2, a2, 10);
            usbPutString(a2);
            usbPutString("       ");
            
            usbPutString("v3 is: ");
            itoa(v3, a3, 10);
            usbPutString(a3);
            usbPutString("       ");
            
            usbPutString("v4 is: ");
            itoa(v4, a4, 10);
            usbPutString(a4);
            usbPutString("       ");
            
            usbPutString("v5 is: ");
            itoa(v5, a5, 10);
            usbPutString(a5);
            usbPutString("       ");
            
            usbPutChar('\r');
            usbPutChar('\n');*/
            
        }
            
            //usbPutChar(12);
        }

        
        
        /* Place your application code here. */
        handle_usb();
        if (flag_KB_string == 1)
        {
            usbPutString(line);         
            flag_KB_string = 0;
        }    
    }   
}
//* ========================================
void usbPutString(char *s)
{
// !! Assumes that *s is a string with allocated space >=64 chars     
//  Since USB implementation retricts data packets to 64 chars, this function truncates the
//  length to 62 char (63rd char is a '!')

#ifdef USE_USB     
    while (USBUART_CDCIsReady() == 0);
    s[63]='\0';
    s[62]='!';
    USBUART_PutData((uint8*)s,strlen(s));
#endif
}
//* ========================================
void usbPutChar(char c)
{
#ifdef USE_USB     
    while (USBUART_CDCIsReady() == 0);
    USBUART_PutChar(c);
#endif    
}
//* ========================================
void handle_usb()
{
    // handles input at terminal, echos it back to the terminal
    // turn echo OFF, key emulation: only CR
    // entered string is made available in 'line' and 'flag_KB_string' is set
    
    static uint8 usbStarted = FALSE;
    static uint16 usbBufCount = 0;
    uint8 c; 
    

    if (!usbStarted)
    {
        if (USBUART_GetConfiguration())
        {
            USBUART_CDC_Init();
            usbStarted = TRUE;
        }
    }
    else
    {
        if (USBUART_DataIsReady() != 0)
        {  
            c = USBUART_GetChar();

            if ((c == 13) || (c == 10))
            {
//                if (usbBufCount > 0)
                {
                    entry[usbBufCount]= '\0';
                    strcpy(line,entry);
                    usbBufCount = 0;
                    flag_KB_string = 1;
                }
            }
            else 
            {
                if (((c == CHAR_BACKSP) || (c == CHAR_DEL) ) && (usbBufCount > 0) )
                    usbBufCount--;
                else
                {
                    if (usbBufCount > (BUF_SIZE-2) ) // one less else strtok triggers a crash
                    {
                       USBUART_PutChar('!');        
                    }
                    else
                        entry[usbBufCount++] = c;  
                }  
            }
        }
    }    
}


/* [] END OF FILE */
